package ir.teamwork.newcoding504;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        File directory = new File(Environment.getExternalStorageDirectory()+File.separator+"aaaaaaaaaaaaaaaaaaaaaaa");
        directory.mkdirs();
        final int[] a = {0};
        Timer timew =new Timer();
        timew.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (a[0] ==1){
                            Intent i=new Intent(SplashScreen.this,MainActivity.class);
                            startActivity(i);
                            finish();
                        }
                        a[0]++;
                    }
                });
            }
        },0,3000);
    }
}
