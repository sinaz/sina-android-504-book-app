package ir.teamwork.newcoding504;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class ViewLessonActivity extends AppCompatActivity {
    Word wordLA;

    //database
    public static final String DBName = "coding-dic";
    public static final String DBLocat = "/data/data/ir.teamwork.newcoding504/databases/";
    private WordDbHelper dbHelper;
    List<Word> flowersList = new ArrayList<>();

    //slide

    ViewPager viewPager;
    LinearLayout layoutDots;
    Button btnNext, btnSkip;
    public String[] word={"food","desk","zoo","cell phone","laptop"};
    public String[] definition={"foodyt fody","desk desk desk","zoo zoo zoo","cell cell cell"," laptop laptop"};
    public String[] mean={"غذا","میز","باغ وحش","تفن همراه","لب تاپ"};
    public String[] code={"غذاغذاغذاغذاغذاغذاغذاغذا",
            "میزمیزمیزمیزمیزمیزمیز",
            "باغ وحشباغ وحشباغ وحشباغ وحشباغ وحش",
            "تفن همراهتفن همراهتفن همراهتفن همراهتفن همراه",
            "لب تاپلب تاپلب تاپلب تاپلب تاپلب تاپلب تاپ"};

    public String[] exampleEn={"when Masoud abandoned his family, the police went looking for him"
            ,"when Masoud abandoned his family, the police went looking for him"
            ,"when Masoud abandoned his family, the police went looking for him"
            ,"when Masoud abandoned his family, the police went looking for him phone"
            ,"when Masoud abandoned his family, the police went looking for him"};

    public String[] exampleFa={"وقتی مسعود خانواده اش را ترك کرد، پلیس به جستجوي او پرداخت"
            ,"وقتی مسعود خانواده اش را ترك کرد، پلیس به جستجوي او پرداخت"
            ,"وقتی مسعود خانواده اش را ترك کرد، پلیس به جستجوي او پرداخت"
            ,"وقتی مسعود خانواده اش را ترك کرد، پلیس به جستجوي او پرداخت"
            ,"وقتی مسعود خانواده اش را ترك کرد، پلیس به جستجوي او پرداخت"};




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_lesson);
        //receive data prev activty
        String newString;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                newString= null;
            } else {
                newString= extras.getString("id");
            }
        } else {
            newString= (String) savedInstanceState.getSerializable("id");
        }
        //Open data base
        dbHelper = new WordDbHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Log.i("database", "database Opened");
        if(db !=null && db.isOpen())
        {
            db.close();
            Log.i("database","database closed.");
        }
        //Database select
        Log.i("id456", newString);
        Log.i("id456", "v= "+flowersList);
        dbHelper.getFlowers(flowersList,newString ,null);

        // slide show
        viewPager = findViewById(R.id.view_pager);
        layoutDots = findViewById(R.id.layoutDots);
        btnNext = findViewById(R.id.btn_next);
        btnSkip = findViewById(R.id.btn_skip);
        viewPager.setAdapter(new SliderPagerAdapter());
        showDots(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //dot
                showDots(position);
                if(position == viewPager.getAdapter().getCount() -1){
                    btnSkip.setVisibility(View.GONE);
                    btnNext.setText(R.string.done );

                }else {
                    btnSkip.setVisibility(View.VISIBLE);
                    btnNext.setText(R.string.next);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentPage = viewPager.getCurrentItem();
                int lastpage = viewPager.getAdapter().getCount()-1;
                if(currentPage == lastpage){
                    finish();
                }else {
                    viewPager.setCurrentItem(currentPage +1);
                }
            }
        });
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });




    }



    private void lunchMainScreen(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();

    }
    private void showDots(int pageNumber){
        TextView[] dot_tv = new TextView[viewPager.getAdapter().getCount()];
        layoutDots.removeAllViews();
        for(int i = 0; i < dot_tv.length;i++){
            dot_tv[i]= new TextView(this);
            dot_tv[i].setText(Html.fromHtml("&#8226;"));
            dot_tv[i].setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
            dot_tv[i].setTextColor(ContextCompat.getColor(this,
                    (i == pageNumber? R.color.dot_active : R.color.dot_inactive)));
            layoutDots.addView(dot_tv[i]);
        }
    }


    public class SliderPagerAdapter extends PagerAdapter {
        String[] slideWord;
        String[] slideDefin;
        String[] slideMean;
        String[] slideCode;
        String[] slideExampleEn;
        String[] slideExampleFA;


        int[] bgColorIds = {R.color.slide_1_bg_color};




        public SliderPagerAdapter(){
            slideWord = word;
            slideDefin = definition;
            slideMean = mean;
            slideCode = code;
            slideExampleEn = exampleEn;
            slideExampleFA = exampleFa;

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(ViewLessonActivity.this)
                    .inflate(R.layout.slide,container,false);
            //color
            view.findViewById(R.id.bgLayout).setBackgroundColor(
                    ContextCompat.getColor(ViewLessonActivity.this,
                            bgColorIds[0]));
            //image
            Resources res=getResources();
            String mDraw=dbHelper.wordArr.get(position).trim();
            int resiD=res.getIdentifier(mDraw,"drawable",getPackageName());
            if (!mDraw.isEmpty()) {
                Log.i("526589",mDraw);
                try {
                    Drawable drawable=res.getDrawable(resiD);
                    ((ImageView)view.findViewById(R.id.slide_image)).setImageDrawable(drawable);
                }catch (Exception e){
                    e.getStackTrace();
                }

            }

            //
            ((TextView)view.findViewById(R.id.slide_word)).append(dbHelper.wordArr.get(position));
            ((TextView)view.findViewById(R.id.slide_defin)).append(dbHelper.definArr.get(position));
            ((TextView)view.findViewById(R.id.slide_code)).append(dbHelper.codeArr.get(position));
            ((TextView)view.findViewById(R.id.slide_mean)).append(dbHelper.meanArr.get(position));
            ((TextView)view.findViewById(R.id.slide_exampleEn)).append(dbHelper.exampleEnArr.get(position));
            ((TextView)view.findViewById(R.id.slide_exampleFa)).append(dbHelper.exampleFaArr.get(position));

            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return dbHelper.cursor.getCount();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
