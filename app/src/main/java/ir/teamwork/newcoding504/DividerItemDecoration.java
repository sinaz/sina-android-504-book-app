package ir.teamwork.newcoding504;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private static int[] ATTRS={
            android.R.attr.listDivider
    };
    Drawable divider;
    int oriention;
    public DividerItemDecoration (Context context , int oriention){
        TypedArray arr=context.obtainStyledAttributes(ATTRS);
        divider = arr.getDrawable(0);
        arr.recycle();
        setOrientaion(oriention);
    }

    private void setOrientaion(int orientiona) {
        if(orientiona != LinearLayoutManager.VERTICAL &&
        orientiona != LinearLayoutManager.HORIZONTAL){
            throw new IllegalArgumentException("wrong oriention");
        }
        this.oriention=orientiona;
    }


    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {

        super.onDrawOver(c, parent, state);

    }

    private void drawVertical(Canvas c, RecyclerView recyclerView) {
        int left = recyclerView.getPaddingLeft();
        int right = recyclerView.getWidth() - recyclerView.getPaddingRight();


        for(int i=0 ; i < recyclerView.getChildCount();i++){
            View childView = recyclerView.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) childView.getLayoutParams();
            int top = childView.getBottom() + params.bottomMargin;
            int bottom = top + divider.getIntrinsicHeight();
            divider.setBounds(left,top,right,bottom);
            divider.draw(c);
        }
    }

    private void drawHorizontal(Canvas c, RecyclerView recyclerView) {
        int top = recyclerView.getPaddingTop();
        int bottom = recyclerView.getHeight() - recyclerView.getPaddingBottom();
        for(int i=0 ; i < recyclerView.getChildCount();i++){
            View childView = recyclerView.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) childView.getLayoutParams();
            int left = childView.getRight() + params.rightMargin;
            int right = left + divider.getIntrinsicWidth();
            divider.setBounds(left,top,right,bottom);
            divider.draw(c);
        }
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        if(oriention==LinearLayoutManager.VERTICAL){
            outRect.set(0,0,0,divider.getIntrinsicWidth());

        }else if(oriention==LinearLayoutManager.HORIZONTAL){
            outRect.set(0,0,divider.getIntrinsicWidth(),0);
        }
        super.getItemOffsets(outRect, view, parent, state);


    }
}


