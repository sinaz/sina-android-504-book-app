package ir.teamwork.newcoding504;

public class MyListLesson {
    private String name;
    private String number;

    public MyListLesson(String name , String number){
    this.name = name;
    this.number = number;
    }

    public String getName(){
        return this.name;
    }
    public String getNumber(){
        return this.number;
    }

    public void setName(String name){
        this.name = name;
    }
    public void setNumber(String number){
        this.number = number;
    }

    @Override
    public String toString() {
        return name + " ( "+ number + " ) ";
    }
}
