package ir.teamwork.newcoding504;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    Button btn;
    public static final String DBName = "coding-dic";
    public static final String DBLocat = "/data/data/ir.teamwork.newcoding504/databases/";
Context context1=MainActivity.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        copyDataBase1(DBLocat+DBName);

        final Timer timer =new Timer();
        final File database = getApplicationContext().getDatabasePath("coding-dic");
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(false == database.exists() ){
                            //Copy db
                            if(copyDatabase(MainActivity.this)){
                                Toast.makeText(MainActivity.this, "دیتا بیس با موفقیت وارد شد.", Toast.LENGTH_SHORT).show();
                                timer.cancel();
                                startActivity(new Intent(MainActivity.this,LessonActivity.class));
                                finish();

                            }else {
                                Toast.makeText(MainActivity.this, "مشکل در وارد کردن دیتا بیس", Toast.LENGTH_SHORT).show();
                                timer.cancel();
                                return;
                            }
                        }else {
                            timer.cancel();
                            startActivity(new Intent(MainActivity.this,LessonActivity.class));
                            finish();
                        }
                    }
                });
            }
        },1,1000);


        //copyDatabase




    }

    private void copyDataBase1(String dbPath){
        try{
            InputStream assestDB = context1.getAssets().open(DBName);
            Log.i("54564456564564564564","    1111    "+assestDB);

            OutputStream appDB = new FileOutputStream(dbPath,false);
            Log.i("54564456564564564564","    1111    ");

            byte[] buffer = new byte[1024];
            int length;
            while ((length = assestDB.read(buffer)) > 0) {
                appDB.write(buffer, 0, length);
            }

            appDB.flush();
            appDB.close();
            assestDB.close();
        }catch(IOException e){
            Log.i("54564456564564564564",e+"    1111    ");
            e.printStackTrace();
        }

    }
    private boolean copyDatabase(Context context){
        String datad = "data";
        try{
            InputStream inputStream = context.getAssets().open("coding-dic");
            String outFileName =  DBLocat+DBName;
            OutputStream outputStream = new FileOutputStream(outFileName,false);
            byte[] buff = new byte[512];
            int length ;
            while ((length = inputStream.read(buff))>0){
                outputStream.write(buff,0,length);
        }
            outputStream.flush();
            outputStream.close();
            inputStream.close();

            Log.v("MainActivty","DB Copied");
            return true;


        }catch (Exception e){
            e.printStackTrace();
            Log.i("54564456564564564564",e+"");
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add("درباره برنامه");
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Toast.makeText(MainActivity.this, "this is about", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
