package ir.teamwork.newcoding504;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    public Context context;


    private List<MyListLesson> myList;
    public MyAdapter( List<MyListLesson> friends){
        if(myList != null){
            this.myList = new ArrayList<>();
        }else{
            this.myList = friends;
        }

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemview = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.my_lesson_list_item, viewGroup,false);
        context = viewGroup.getContext();

        return new MyViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(myList.get(i),context);

    }

    @Override
    public int getItemCount() {
        Log.i("321",""+ myList.size());

        return myList.size();
    }




    static class MyViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView tvName,tvCity;
        Context context ;
        RelativeLayout rl_layout;

         MyViewHolder(View itemView){
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvCity = itemView.findViewById(R.id.tv_city);
            rl_layout = itemView.findViewById(R.id.rel_layout);

        }
        public void bind (final MyListLesson myList, final Context context){
             this.context=context;
            tvCity.setText(myList.getNumber());
            tvName.setText(myList.getName());

            rl_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("252525",myList.getNumber());
                    Intent intent = new Intent(context.getApplicationContext(),ViewLessonActivity.class);
                   intent.putExtra("id",myList.getNumber());
                    context.startActivity(intent);
                }
            });
        }

    }

}
