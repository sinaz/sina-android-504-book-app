package ir.teamwork.newcoding504;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class LessonActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<MyListLesson> myList;
    MyAdapter adapter;
    TextView aboutwe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);
        aboutwe=findViewById(R.id.aboutwe);
        aboutwe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder al=new AlertDialog.Builder(LessonActivity.this);
                al.setTitle("درباره ما");
                al.setMessage("ساخته شده توسط عرفان امیرپور");
                al.setNegativeButton("باشه", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                   al.setCancelable(false);
                    }
                });
                al.show();
            }
        });
        recyclerView = findViewById(R.id.recyclerView);
        prepareDate();
        showDate();



    }
    private void showDate() {
        adapter = new MyAdapter (myList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
//        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));
//        recyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.HORIZONTAL));
//        recyclerView.setLayoutManager(new GridLayoutManager(this,2)); //نمایش چند ستونی

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
    private void prepareDate() {
        if(myList == null){
            myList = new ArrayList<>();


        }else{
            myList.clear();
        }
        myList.add(new MyListLesson("Lesson One","1"));
        myList.add(new MyListLesson("Lesson Two","2"));
        myList.add(new MyListLesson("Lesson Tree","3"));
        myList.add(new MyListLesson("Lesson four","4"));
        myList.add(new MyListLesson("Lesson Five","5"));
        myList.add(new MyListLesson("Lesson Six","6"));
        myList.add(new MyListLesson("Lesson Seven","7"));
        myList.add(new MyListLesson("Lesson Eight","8"));
        myList.add(new MyListLesson("Lesson Nine","9"));
        myList.add(new MyListLesson("Lesson Ten","10"));
        myList.add(new MyListLesson("Lesson Eleven","11"));
        myList.add(new MyListLesson("Lesson Twevle","12"));
        myList.add(new MyListLesson("Lesson Thirteen","13"));
        myList.add(new MyListLesson("Lesson Fourteen","14"));
        myList.add(new MyListLesson("Lesson fifteen","15"));
        myList.add(new MyListLesson("Lesson Sixteen","16"));
        myList.add(new MyListLesson("Lesson Seventeen","17"));
        myList.add(new MyListLesson("Lesson Eighteen","18"));
        myList.add(new MyListLesson("Lesson Ninteen","19"));
        myList.add(new MyListLesson("Lesson Twenty","20"));
        myList.add(new MyListLesson("Lesson Twenty One","21"));
        myList.add(new MyListLesson("Lesson Twenty Two","22"));
        myList.add(new MyListLesson("Lesson Twenty Tree","23"));
        myList.add(new MyListLesson("Lesson Twenty four","24"));
        myList.add(new MyListLesson("Lesson Twenty Five","25"));
        myList.add(new MyListLesson("Lesson Twenty Six","26"));
        myList.add(new MyListLesson("Lesson Twenty Seven","27"));
        myList.add(new MyListLesson("Lesson Twenty Eight","28"));
        myList.add(new MyListLesson("Lesson Twenty Nine","29"));
        myList.add(new MyListLesson("Lesson Thirty","30"));
        myList.add(new MyListLesson("Lesson Thirty One","31"));
        myList.add(new MyListLesson("Lesson Thirty Two","32"));
        myList.add(new MyListLesson("Lesson Thirty Tree","33"));
        myList.add(new MyListLesson("Lesson Thirty four","34"));
        myList.add(new MyListLesson("Lesson Thirty Five","35"));
        myList.add(new MyListLesson("Lesson Thirty Six","36"));
        myList.add(new MyListLesson("Lesson Thirty Seven","37"));
        myList.add(new MyListLesson("Lesson Thirty Eight","38"));
        myList.add(new MyListLesson("Lesson Thirty Nine","39"));
        myList.add(new MyListLesson("Lesson Forty","40"));
        myList.add(new MyListLesson("Lesson Forty One","41"));
        myList.add(new MyListLesson("Lesson Forty Two","42"));


    }

}
