package ir.teamwork.newcoding504;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pooya Nazari on 4/10/2019.
 */

public class WordDbHelper extends SQLiteOpenHelper {
    ArrayList<String> wordArr = new ArrayList<String>();;
    ArrayList<String> meanArr = new ArrayList<String>();
    ArrayList<String> definArr = new ArrayList<String>();
    ArrayList<String> lessonArr = new ArrayList<String>();
    ArrayList<String> codeArr = new ArrayList<String>();
    ArrayList<String> exampleEnArr = new ArrayList<String>();
    ArrayList<String> exampleFaArr = new ArrayList<String>();
    Cursor cursor;
    Word wordLA;
    public static final String DB_NAME = "coding-dic";
    private static final int DB_VERSION = 1;
    public static final String TABLE_DICTIONARY = "Sheet1_";
//    private static finall String[] allColumns= {Word.KEY_ID, Word.KEY_CAT, Word.KEY_NAME};






    public WordDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //deleted
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       //backup

        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_DICTIONARY);
        Log.i("database9090", "table dropped.");
        onCreate(db);

    }

//    public void insertFlowerToDB(Word flower , Context context){
//        boolean boolianCheck = true;
//        SQLiteDatabase dbReadOnly  = getReadableDatabase();
//        Cursor cursorCheck = dbReadOnly.rawQuery("SELECT * FROM  '" + TABLE_DICTIONARY + "'  ",null);
//        Long idCheck;
//        if(cursorCheck.moveToFirst()){
//            do{
//                idCheck = (cursorCheck.getLong(cursorCheck.getColumnIndex(Word.KEY_ID)));
//                if(idCheck == flower.getPriductId()){
//                    Log.i("399",idCheck+"__>"+flower.getPriductId());
//                    boolianCheck = false;
//                }
//            }while (cursorCheck.moveToNext());
//        }
//        cursorCheck.close();
//        if(dbReadOnly.isOpen()) dbReadOnly.close();
//
//        if(boolianCheck) {
//            MainInsert(flower);
//        }
//
//    }

//    private void MainInsert(Word flower) {
//            /////////////////////////////////////////////////////////////////////// start
//            // این کد های insert است
////        if(getFlowers(Word.KEY_ID + " = " + flower.getPriductId(),null).isEmpty()){
//            SQLiteDatabase db = getWritableDatabase();
//            String INSERTA = "INSERT INTO '" + TABLE_DICTIONARY + "'('" +
//                    Word.KEY_ID + "','" +
//                    Word.KEY_CAT + "','" +
//                    Word.KEY_NAME + "') VALUES ('" +
//                    flower.getPriductId() + "','" +
//                    flower.getCategory() + "','" +
//                    flower.getName() + "')";
//            try {
//                db.execSQL(INSERTA);
//                Log.i("execSQL Er 555", "-----------------------------------------------_____________");
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.i("execSQL Er 566", "-----------------------------------------------_____________" + e);
//            }
//            if (db.isOpen()) db.close();
////        }
//    }


    public List<Word> getAllFlowers(){
        SQLiteDatabase db  = getReadableDatabase();
        List<Word> flowerList = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM  '" + TABLE_DICTIONARY +
                "'  ",null);

        Log.i("Databse1","retuned: "+cursor.getCount());
        if(cursor.moveToFirst()){
            do{
                // process for each raw

                //جایگزین چند خط پایین
                Word flower = Word.curserToFlower(cursor);
//                Word flower = new Word();
//                flower.setName(cursor.getString(cursor.getColumnIndex(Word.KEY_NAME) ));
//                flower.setPriductId(cursor.getLong(cursor.getColumnIndex(Word.KEY_ID)));
//                flower.setCategory(cursor.getString(cursor.getColumnIndex(Word.KEY_CAT)));
//                flower.setPrice(cursor.getShort(cursor.getColumnIndex(Word.KEY_PRICE)));
//                flower.setPhoto(cursor.getString(cursor.getColumnIndex(Word.KEY_PHOTO)));
                Log.i("Databse1","retuned: "+cursor.getCount());
                flowerList.add(flower);
            }while (cursor.moveToNext());
        }
        cursor.close();


        if(db.isOpen()) db.close();
        return flowerList;
    }
//    public void update(long id, ContentValues values, String name, String category){
//        SQLiteDatabase db = getWritableDatabase();
//        Log.i("655",id+"-"+ TABLE_DICTIONARY +values+ Word.KEY_ID + "=" + id);
//        String SqlCmd = "UPDATE '"+ TABLE_DICTIONARY +"' SET "+ Word.KEY_NAME+"='"+ name +"', " + Word.KEY_CAT +"='" + category + "' WHERE productId=" + id + "";
//
//
////        db.execSQL("UPDATE '"+TABLE_DICTIONARY+"' SET "+Word.KEY_NAME+"='"+ name +"'," + Word.KEY_PRICE + "='" + price + "', " + Word.KEY_CAT +"='" + category + "' WHERE productId=" + id + "");
//        db.execSQL(SqlCmd);
////        int count = db.update(TABLE_DICTIONARY, values,Word.KEY_ID + "=" + id,null );
//        Log.i("up777",""+db+"----"+SqlCmd);
//        if(db.isOpen()){
//            db.close();
//        }
//
//
//    }

//    public List<Word> getFlowers(String selection, String[] selArgs){
//        SQLiteDatabase db  = getReadableDatabase();
//        List<Word> flowerList = new ArrayList<>();
//        String cmdSearch = "SELECT * FROM  '" + TABLE_DICTIONARY + "' WHERE name LIKE '"+ selection +"%'  ";
//        Cursor cursor = db.rawQuery(cmdSearch,null);
//        Log.i("7777","retuned: "+cursor.getCount()+cmdSearch);
//        if(cursor.moveToFirst()){
//            do{
//                Word flower = Word.curserToFlower(cursor);
//                Log.i("7877","retuned: "+cursor.getCount());
//                flowerList.add(flower);
//            }while (cursor.moveToNext());
//        }
//        cursor.close();
//        if(db.isOpen()) db.close();
//        return flowerList;
//    }






    public void getFlowers(List<Word> flowerList, String selection, String[] selArgs){
        StringBuffer buffer = new StringBuffer();
        SQLiteDatabase db  = getReadableDatabase();
        flowerList.clear();
        //دستور Select
//        String cmdSearch = "SELECT * FROM  '" + TABLE_DICTIONARY + "' WHERE lesson '"+ selection +"'  ";
        String cmdSearch = "SELECT * FROM  '" + TABLE_DICTIONARY + "' WHERE lesson ='"+ selection +"' ";
         cursor = db.rawQuery(cmdSearch,null);

        Log.i("2580","retuned: "+cursor.getCount()+"\n cmd:"+cmdSearch);

        if(cursor.moveToFirst()){
            do{
//                Word flower = Word.curserToFlower(cursor);
                Log.i("7877","retuned: "+cursor.getCount());
                while(!cursor.isAfterLast()) {
                    wordArr.add(cursor.getString(cursor.getColumnIndex("word")));
                    definArr.add(cursor.getString(cursor.getColumnIndex("definition")));
                    codeArr.add(cursor.getString(cursor.getColumnIndex("code")));
                    meanArr.add(cursor.getString(cursor.getColumnIndex("mean")));
                    lessonArr.add(cursor.getString(cursor.getColumnIndex("lesson")));
                    exampleEnArr.add(cursor.getString(cursor.getColumnIndex("exampleEn")));
                    exampleFaArr.add(cursor.getString(cursor.getColumnIndex("exampleFa")));
                    cursor.moveToNext();
                }


//                flowerList.add(flower);
            }while (cursor.moveToNext());
//            Log.i("9898",""+wordLA.wordArr);
//            Log.i("9898",""+wordLA.wordArr.get(20));
//            Log.i("9898",""+wordLA.wordArr.get(21));

        }
        cursor.close();
        if(db.isOpen()) db.close();
    }


}

