package ir.teamwork.newcoding504;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by Pooya Nazari on 4/10/2019.
 */

public class Word {

    public  static final String KEY_Word = "word";
    public static final String KEY_Defin= "definition";
    public static final String KEY_Code= "code";
    public static final String KEY_Mean= "mean";
    public static final String KEY_Lesson= "lesson";
    public static final String KEY_ExampleEn= "exampleEn";
    public static final String KEY_ExampleFa= "exampleFa";

    private String word;
    private String defin;
    private String code;
    private String mean;
    private String lesson;
    private String exampleEn;
    private String exampleFA;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDefin() {
        return defin;
    }

    public void setDefin(String defin) {
        this.defin = defin;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMean() {
        return mean;
    }

    public void setMean(String mean) {
        this.mean = mean;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getExampleEn() {
        return exampleEn;
    }

    public void setExampleEn(String exampleEn) {
        this.exampleEn = exampleEn;
    }

    public String getExampleFA() {
        return exampleFA;
    }

    public void setExampleFA(String exampleFA) {
        this.exampleFA = exampleFA;
    }

    @Override
    public String toString() {
        return word;
    }





    public ContentValues getContentValuesForDb(){
        ContentValues values = new ContentValues();
        values.put(Word.KEY_Word,word);
        values.put(Word.KEY_Defin,defin);
        values.put(Word.KEY_Mean,mean);
        values.put(Word.KEY_Code,code);
        values.put(Word.KEY_Lesson,lesson);
        values.put(Word.KEY_ExampleEn,exampleEn);
        values.put(Word.KEY_ExampleFa,exampleFA);

        return values;
    }

    public static Word curserToFlower(Cursor courser){
        Word flower = new Word();
        flower.setWord(courser.getString(courser.getColumnIndex(KEY_Word)));
        flower.setDefin(courser.getString(courser.getColumnIndex(KEY_Defin)));
        flower.setMean(courser.getString(courser.getColumnIndex(KEY_Mean)));
        flower.setCode(courser.getString(courser.getColumnIndex(KEY_Code)));
        flower.setLesson(courser.getString(courser.getColumnIndex(KEY_Lesson)));
        flower.setExampleEn(courser.getString(courser.getColumnIndex(KEY_ExampleEn)));
        flower.setExampleFA(courser.getString(courser.getColumnIndex(KEY_ExampleFa)));
        return flower;
    }
}
